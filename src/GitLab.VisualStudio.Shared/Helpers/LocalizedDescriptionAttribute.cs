﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.VisualStudio.Shared.Helpers
{
    public class EnumHelper
    {
        public static string GetDescription(Enum EnumValue)
        {
            FieldInfo fieldInfo = EnumValue.GetType().GetField(EnumValue.ToString());
            if (null == fieldInfo)
            {
                return "";
            }

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo
                .GetCustomAttributes(typeof(DescriptionAttribute), true);

            string descriptinon = "";
            foreach (DescriptionAttribute attr in attributes)
            {
                descriptinon = descriptinon + Environment.NewLine + attr.Description;
            }
            return descriptinon.Trim();
        }

        public static string GetLocalizedDescription(Enum EnumValue)
        {
            FieldInfo fieldInfo = EnumValue.GetType().GetField(EnumValue.ToString());
            LocalizedDescriptionAttribute[] attributes = (LocalizedDescriptionAttribute[])fieldInfo
                .GetCustomAttributes(typeof(LocalizedDescriptionAttribute), true);

            string descriptinon = EnumValue.ToString();

            if (1 == attributes.Length)
            {
                descriptinon = attributes[0].Description;
            }
            return descriptinon;
        }
    }

    public class LocalizedDescriptionAttribute: DescriptionAttribute
    {
        static string Localize(string key)
        {
            return Strings.ResourceManager.GetString(key);
        }

        public LocalizedDescriptionAttribute(string key)
            : base(Localize(key))
        {
        }
    }
}
