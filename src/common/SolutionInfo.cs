﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("GitLab Extension for Visual Studio")]
[assembly: ComVisible(false)]
[assembly: AssemblyCompany("uso")]
[assembly: AssemblyCopyright("Copyright (c) uso 2018")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]