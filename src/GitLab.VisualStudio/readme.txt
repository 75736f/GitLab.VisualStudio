﻿GitLab Extension for Visual Studio

 
The GitLab Extension for Visual Studio provides GitLab integration in Visual Studio 2017. Most of the extension UI lives in the Team Explorer pane, which is available from the View menu.


Build requirements

Visual Studio 2017
Visual Studio SDK
Build

Clone the repository and its submodules in a git GUI client or via the command line:

git clone https://gitlab.com/maikebing/GitLab.VisualStudio
cd VisualStudio

Open the GitLabVS.sln solution with Visual Studio 2017.  

 
Contributing

Visit the Contributor Guidelines for details on how to contribute as well as the Open Code of Conduct for details on how to participate.

Copyright

Copyright 2015 - 2017 MaiKeBing, Inc.
Copyright 2018 uso

Licensed under the MIT License