﻿using System.Collections.Generic;
using System.ComponentModel;
using GitLab.VisualStudio.Shared.Models;

namespace GitLab.VisualStudio.Shared
{
    // Keep in Sync with NGitLab.Impl.Api.ApiVersion!
    public enum ApiVersion
    {
        [Description("GitLab ApiV3")]
        V3,
        [Description("GitLab ApiV4")]
        V4,
        [Description("GitLab ApiV3 OAuth2")]
        V3_OAuth,
        [Description("GitLab ApiV4 OAuth2")]
        V4_OAuth
    }

    public enum ProjectListType
    {
        Accessible,
        Owned,
        Membership,
        Starred,
        Forked
    }

    public interface IWebService
    {
        User LoginAsync(bool enable2fa, string host, string email, string password, ApiVersion apiVersion, bool IgnoreSslValidationErrors);

        IReadOnlyList<Project> GetProjects();

        CreateProjectResult CreateProject(string name, string description, string VisibilityLevel, int namespaceid);

        CreateProjectResult CreateProject(string name, string description, string VisibilityLevel);

        CreateSnippetResult CreateSnippet(string title, string filename, string description, string code, string visibility);

        Project GetActiveProject();

        Project GetActiveProject(ProjectListType projectListType);

        IReadOnlyList<Project> GetProjects(ProjectListType projectListType);

        Project GetProject(string namespacedpath);

        IReadOnlyList<NamespacesPath> GetNamespacesPathList();
    }
}