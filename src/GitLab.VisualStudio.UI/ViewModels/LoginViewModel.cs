﻿using GitLab.VisualStudio.Shared;
using GitLab.VisualStudio.Shared.Helpers;
using GitLab.VisualStudio.Shared.Helpers.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace GitLab.VisualStudio.UI.ViewModels
{
    public interface IPasswordMediator
    {
        string Password { get; set; }
    }

    public class LoginViewModel : Validatable
    {
        private IPasswordMediator _mediator;

        private readonly IDialog _dialog;
        private readonly IMessenger _messenger;
        private readonly IShellService _shell;
        private readonly IStorage _storage;
        private readonly IWebService _web;

        public LoginViewModel(IDialog dialog, IPasswordMediator mediator, IMessenger messenger, IShellService shell, IStorage storage, IWebService web)
        {
            _dialog = dialog;
            _messenger = messenger;
            _shell = shell;
            _storage = storage;
            _web = web;

            _mediator = mediator;

            ApiVersions = new Dictionary<ApiVersion, string>();
            foreach(ApiVersion version in Enum.GetValues(typeof(ApiVersion)))
            {
                ApiVersions.Add(version, EnumHelper.GetDescription(version));
            }
            SelectedApiVersion = ApiVersion.V4;

            _ignoreSslValidationErrors = false;

            _loginCommand = new DelegateCommand(OnLogin);
            _forgetPasswordCommand = new DelegateCommand(OnForgetPassword);
            _activeAccountCommand = new DelegateCommand(OnActiveAccount);
            _signupCommand = new DelegateCommand(OnSignup);
        }

        private string _host;
        [Required(ErrorMessageResourceType = typeof(Strings), AllowEmptyStrings = false, ErrorMessageResourceName = "Login_HostIsRequired")]
        public string Host
        {
            get
            {
                if (string.IsNullOrEmpty(_host) || string.IsNullOrWhiteSpace(_host))
                {
                    _host = Strings.DefaultHost;
                }
                return _host;
            }
            set
            {
                try
                {
                    string tmpurl = value;
                    if (value.StartsWith("git@"))
                    {
                        var ary = value.Split('@', ':', '/');
                        tmpurl = $"https://{ary[2]}@{ary[1]}";
                    }
                    var urlhost = new UriBuilder(tmpurl);
                    if (!string.IsNullOrEmpty(urlhost.UserName))
                    {
                        Email = urlhost.UserName;
                        urlhost.UserName = "";
                    }
                    if (!string.IsNullOrEmpty(urlhost.Password))
                    {
                        Password = urlhost.Password;
                        urlhost.Password = "";
                    }
                    SetProperty(ref _host, urlhost.Uri.ToString());
                }
                catch (Exception ex)
                {
                    _dialog.Error(ex.Message);
                }
            }
        }

        private string _email;
        [Required(ErrorMessageResourceType = typeof(Strings), AllowEmptyStrings = true, ErrorMessageResourceName = "Login_EmailIsRequired")]
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private bool _Enable2FA;
        public bool Enable2FA
        {
            get { return _Enable2FA; }
            set { SetProperty(ref _Enable2FA, value); }
        }

        [Required(ErrorMessageResourceType = typeof(Strings), ErrorMessageResourceName = "Login_PasswordIsRequired")]
        public string Password
        {
            get { return _mediator?.Password; }
            set
            {
                ValidateProperty(value);

                // Do not store the password in a private field as it should not be stored in memory in plain-text.
                // Instead, the supplied PasswordAccessor serves as the backing store for the value.

                OnPropertyChanged();
            }
        }

        public IDictionary<ApiVersion, string> ApiVersions { get; }

        private ApiVersion _apiversion;
        public ApiVersion SelectedApiVersion
        {
            get { return _apiversion; }
            set { SetProperty(ref _apiversion, value); }
        }

        private bool _ignoreSslValidationErrors;
        public bool IgnoreSslValidationErrors
        {
            get { return _ignoreSslValidationErrors; }
            set { SetProperty(ref _ignoreSslValidationErrors, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private string _busyContent;
        public string BusyContent
        {
            get { return _busyContent; }
            set { SetProperty(ref _busyContent, value); }
        }

        private DelegateCommand _loginCommand;
        public ICommand LoginCommand
        {
            get { return _loginCommand; }
        }

        private DelegateCommand _forgetPasswordCommand;
        public ICommand ForgetPasswordCommand
        {
            get { return _forgetPasswordCommand; }
        }

        private DelegateCommand _activeAccountCommand;
        public ICommand ActiveAccountCommand
        {
            get { return _activeAccountCommand; }
        }

        private DelegateCommand _signupCommand;
        public ICommand SignupCommand
        {
            get { return _signupCommand; }
        }

        private void OnLogin()
        {
            if(string.IsNullOrEmpty(Email) && (ApiVersion.V4 == SelectedApiVersion))
            {
                // Prevent the validator from failing the login for Api V4 caused by the missing username/e-mail.
                Email = "Jane Doe";
            }

            Validate();
            if (HasErrors)
            {
                return;
            }
            IsBusy = true;
            BusyContent = Strings.Common_Loading;
            var successed = false;
            Exception exlogin = null;
            Task.Run(() =>
            {
                try
                {
                    var user = _web.LoginAsync(Enable2FA, Host, Email, Password, SelectedApiVersion, IgnoreSslValidationErrors);
                    if (null != user)
                    {
                        successed = true;
                        user.Host = Host;
                        _storage.SaveUser(user, Password);
                    }
                }
                catch (Exception ex)
                {
                    exlogin = ex;
                }
            }).ContinueWith(task =>
            {
                IsBusy = false;
                BusyContent = null;
                if (successed)
                {
                    _dialog.Close();
                    _messenger.Send("OnLogined");
                }
                else
                {
                    MessageBox.Show(Strings.Login_FailedToLogin 
                        + System.Environment.NewLine 
                        + exlogin.Message);
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void OnForgetPassword()
        {
            _shell.OpenUrl($"{Host}/users/password/new");
        }

        private void OnActiveAccount()
        {
            _shell.OpenUrl($"{Host}/users/confirmation/new");
        }

        private void OnSignup()
        {
            _shell.OpenUrl($"{Host}/users/sign_in#register-pane");
        }
    }
}